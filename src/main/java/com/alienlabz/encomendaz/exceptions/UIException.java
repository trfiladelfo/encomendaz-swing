package com.alienlabz.encomendaz.exceptions;

/**
 * Exceção lançada sempre que ocorrem erros na interface do sistema.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class UIException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public UIException() {
		super();
	}

	public UIException(final String arg0) {
		super(arg0);
	}

	public UIException(final String arg0, final Throwable arg1) {
		super(arg0, arg1);
	}

	public UIException(final Throwable arg0) {
		super(arg0);
	}

}
