package com.alienlabz.encomendaz.bus.events;

/**
 * Evento lançado para informar que a rede está no ar.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class NetworkOn {

}
