package com.alienlabz.encomendaz.bus.events;

import com.alienlabz.encomendaz.domain.Person;

/**
 * Evento lançado quando se quer gravar as pessoas que são destinatário e remetente padrão.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class SetDefaultFromTo {
	private Person from;
	private Person to;

	public SetDefaultFromTo(Person from, Person to) {
		this.from = from;
		this.to = to;
	}

	public void setFrom(Person from) {
		this.from = from;
	}

	public Person getFrom() {
		return from;
	}

	public void setTo(Person to) {
		this.to = to;
	}

	public Person getTo() {
		return to;
	}
}
