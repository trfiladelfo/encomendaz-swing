package com.alienlabz.encomendaz.bus.events;

/**
 * Evento lançado quando há a necessidade de exibir os rastreamentos que não foram registrados ainda.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class ListUnregistered {

}
