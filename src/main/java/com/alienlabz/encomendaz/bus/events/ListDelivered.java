package com.alienlabz.encomendaz.bus.events;

/**
 * Evento lançado quando há a necessidade de exibir os rastreamentos já entregues.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class ListDelivered {

}
