package com.alienlabz.encomendaz.view.window;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.events.SetDefaultFromTo;
import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.view.model.PersonComboBoxModel;

import net.miginfocom.swing.MigLayout;

/**
 * Tela para a escolha do destinatário e remetente padrão.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class DefaultFromTo extends JDialog {
	private JPanel panel;
	private JComboBox from;
	private JComboBox to;
	private ResourceBundle bundleMessages;

	public DefaultFromTo() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(400, 150));
		setModal(true);
		setTitle(bundleMessages.getString("default.fromto.title"));
		initialize();
	}

	/**
	 * Inicializar os componentes da tela.
	 */
	private void initialize() {
		panel = new JPanel(new MigLayout("fillx", "[align right][grow]"));
		from = FieldFactory.combo();
		to = FieldFactory.combo();

		panel.add(new JLabel(bundleMessages.getString("dialog.defaultfromto.from.label")), "gap 10");
		panel.add(from, "span, growx");

		panel.add(new JLabel(bundleMessages.getString("dialog.defaultfromto.to.label")), "gap 10");
		panel.add(to, "span, growx");

		JButton button = FieldFactory.button(bundleMessages.getString("default.button.ok"));
		panel.add(button, "span, growx");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(new SetDefaultFromTo((Person) from.getSelectedItem(), (Person) to.getSelectedItem()));
			}

		});

		add(panel, BorderLayout.CENTER);
	}

	/**
	 * Obter o remetente padrão.
	 * 
	 * @return Remetente padrão.
	 */
	public Person getFrom() {
		return (Person) from.getSelectedItem();
	}

	/**
	 * Obter o destinatário padrão.
	 * 
	 * @return Destinatário padrão.
	 */
	public Person getTo() {
		return (Person) to.getSelectedItem();
	}

	/**
	 * Definir quem é o remetente padrão.
	 * 
	 * @param person Remetente Padrão.
	 */
	public void setFrom(Person person) {
		from.setSelectedItem(person);
	}

	/**
	 * Definir quem é o destinatário padrão.
	 * 
	 * @param person Destinatário padrão.
	 */
	public void setTo(Person person) {
		to.setSelectedItem(person);
	}

	/**
	 * Definir a lista de possíveis remetentes.
	 * 
	 * @param list Lista de pessoas que podem ser remetentes.
	 */
	public void setFromList(List<Person> list) {
		PersonComboBoxModel model = new PersonComboBoxModel();
		model.setPeople(list);
		from.setModel(model);
	}

	/**
	 * Definir a lista de possíveis destinatários.
	 * 
	 * @param list Lista de pessoas que podem ser destinatários.
	 */
	public void setToList(List<Person> list) {
		PersonComboBoxModel model = new PersonComboBoxModel();
		model.setPeople(list);
		to.setModel(model);
	}

}
