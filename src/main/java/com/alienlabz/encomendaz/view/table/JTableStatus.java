package com.alienlabz.encomendaz.view.table;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import com.alienlabz.encomendaz.domain.TrackingStatus;
import com.alienlabz.encomendaz.view.model.StatusTableModel;
import com.alienlabz.encomendaz.view.table.cellrenderer.StatusDetailsCellRenderer;


/**
 * Tabela que apresentará a lista de status cadastrados.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class JTableStatus extends JScrollPane {
	private static final long serialVersionUID = 1L;
	private JTable table;

	public JTableStatus() {
		initializeJTable();
		initializeCellProperties();
	}

	/**
	 * Inicializar as propriedades das celulas.
	 */
	private void initializeCellProperties() {
		table.setRowHeight(40);

		table.getColumnModel().getColumn(0).setPreferredWidth(180);
		table.getColumnModel().getColumn(0).setMaxWidth(180);
		table.getColumnModel().getColumn(1).setPreferredWidth(350);
		table.getColumnModel().getColumn(1).setMaxWidth(350);
		table.getColumnModel().getColumn(2).setPreferredWidth(200);
		table.getColumnModel().getColumn(2).setMaxWidth(200);

		table.getColumnModel().getColumn(0).setCellRenderer(new StatusDetailsCellRenderer());
		table.getColumnModel().getColumn(1).setCellRenderer(new StatusDetailsCellRenderer());
		table.getColumnModel().getColumn(2).setCellRenderer(new StatusDetailsCellRenderer());
		table.getColumnModel().getColumn(3).setCellRenderer(new StatusDetailsCellRenderer());

		table.getTableHeader().setPreferredSize(new Dimension(0, 30));
	}

	/**
	 * Inicializar a tabela com suas propriedades padrão.
	 */
	private void initializeJTable() {
		table = new JTable();
		table.setModel(new StatusTableModel(new ArrayList<TrackingStatus>()));

		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setDoubleBuffered(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		setViewportView(table);
		setAutoscrolls(true);
	}

	/**
	 * Definir quais são as situações para exibir.
	 * 
	 * @param statuses Status.
	 */
	public void setStatuses(final List<TrackingStatus> statuses) {
		table.setModel(new StatusTableModel(statuses));
		initializeCellProperties();
	}

}
