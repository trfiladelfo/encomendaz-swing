package com.alienlabz.encomendaz.view.table;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.ResourceBundle;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.Archived;
import com.alienlabz.encomendaz.bus.qualifiers.Browse;
import com.alienlabz.encomendaz.bus.qualifiers.Deleted;
import com.alienlabz.encomendaz.bus.qualifiers.DetailsRequired;
import com.alienlabz.encomendaz.bus.qualifiers.Map;
import com.alienlabz.encomendaz.bus.qualifiers.Modify;
import com.alienlabz.encomendaz.bus.qualifiers.PrintLabel;
import com.alienlabz.encomendaz.bus.qualifiers.SendEmail;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;
import com.alienlabz.encomendaz.view.model.DetailsTableModel;
import com.alienlabz.encomendaz.view.table.cellrenderer.CodeCellRenderer;
import com.alienlabz.encomendaz.view.table.cellrenderer.DefaultCellRenderer;
import com.alienlabz.encomendaz.view.table.cellrenderer.ElapsedCellRenderer;
import com.alienlabz.encomendaz.view.table.cellrenderer.FromToCellRenderer;
import com.alienlabz.encomendaz.view.table.cellrenderer.IconCellRenderer;
import com.alienlabz.encomendaz.view.table.cellrenderer.TimeCellRenderer;
import com.jidesoft.swing.SearchableBar;
import com.jidesoft.swing.TableSearchable;

/**
 * Tabela de exibição de {@link Tracking}s.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class JTableTrackings extends JScrollPane {
	private static final long serialVersionUID = 1L;
	private int selectedRow = -1;
	private JTable table;
	private JPopupMenu popupMenu;
	private ResourceBundle bundleMessages;
	private ResourceBundle bundleImages;
	private SearchableBar _tableSearchableBar;
	private JPanel panel;

	public JTableTrackings() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleImages = ResourceBundleFactory.getImagesBundle();
		initializeJTable();
		initializePopupMenu();

		table.setColumnSelectionAllowed(false);
		table.setRowSelectionAllowed(true);
		TableSearchable searchable = new TableSearchable(table) {
			@Override
			protected String convertElementToString(Object object) {
				if (object != null && object instanceof Tracking) {
					Tracking tracking = (Tracking) object;
					return tracking.getSearchable();
				}
				return "";
			}
		};
		searchable.setCaseSensitive(false);
		searchable.setRepeats(true);
		searchable.setSearchColumnIndices(new int[] { 2 });
		_tableSearchableBar = SearchableBar.install(searchable,
				KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.CTRL_DOWN_MASK), new SearchableBar.Installer() {
					public void openSearchBar(SearchableBar searchableBar) {
						JTableTrackings.this.panel.add(searchableBar, BorderLayout.SOUTH);
						JTableTrackings.this.panel.invalidate();
						JTableTrackings.this.panel.revalidate();
					}

					public void closeSearchBar(SearchableBar searchableBar) {
						JTableTrackings.this.panel.remove(searchableBar);
						JTableTrackings.this.panel.invalidate();
						JTableTrackings.this.panel.revalidate();
					}
				});
		_tableSearchableBar.setName("TableSearchableBar");
	}

	/**
	 * Criar o menu popup.
	 */
	private void initializePopupMenu() {
		popupMenu = new JPopupMenu();

		JMenuItem menuItem = new JMenuItem(bundleMessages.getString("popup.table.modify"));
		menuItem.setName("modify");
		menuItem.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.popup.table.modify")));
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final DetailsTableModel pModel = (DetailsTableModel) table.getModel();
				final Tracking tracking = pModel.getTracking(selectedRow);
				EventManager.fire(tracking, new AnnotationLiteral<Modify>() {
				});
			}

		});
		popupMenu.add(menuItem);

		menuItem = new JMenuItem(bundleMessages.getString("popup.table.delete"));
		menuItem.setName("remove");
		menuItem.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.popup.table.delete")));
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int resp = JOptionPane.showConfirmDialog(null, bundleMessages.getString("generic.confirm.exclusion"),
						bundleMessages.getString("generic.confirm.title"), JOptionPane.YES_NO_OPTION);
				if (resp == 0) {
					final DetailsTableModel pModel = (DetailsTableModel) table.getModel();
					final Tracking tracking = pModel.getTracking(selectedRow);
					EventManager.fire(tracking, new AnnotationLiteral<Deleted>() {
					});
				}
			}

		});
		popupMenu.add(menuItem);

		menuItem = new JMenuItem(bundleMessages.getString("popup.table.sendmail"));
		menuItem.setName("sendmail");
		menuItem.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.popup.table.sendmail")));
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final DetailsTableModel pModel = (DetailsTableModel) table.getModel();
				final Tracking tracking = pModel.getTracking(selectedRow);
				EventManager.fire(tracking, new AnnotationLiteral<SendEmail>() {
				});				
			}

		});
		popupMenu.add(menuItem);

		//		menuItem = new JMenuItem(bundleMessages.getString("popup.table.sendtwitter"));
		//		menuItem.setName("sendtwitter");
		//		menuItem.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.popup.table.sendtwitter")));
		//		menuItem.addActionListener(new ActionListener() {
		//
		//			@Override
		//			public void actionPerformed(ActionEvent e) {
		//
		//			}
		//
		//		});
		//		popupMenu.add(menuItem);
		menuItem = new JMenuItem(bundleMessages.getString("popup.table.print.label"));
		menuItem.setName("etiqueta");
		menuItem.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.popup.table.print.label")));
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final DetailsTableModel pModel = (DetailsTableModel) table.getModel();
				final Tracking tracking = pModel.getTracking(selectedRow);
				EventManager.fire(tracking, new AnnotationLiteral<PrintLabel>() {
				});
			}

		});
		popupMenu.add(menuItem);

		menuItem = new JMenuItem(bundleMessages.getString("popup.table.archive"));
		menuItem.setName("archive");
		menuItem.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.popup.table.archive")));
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int resp = JOptionPane.showConfirmDialog(null, bundleMessages.getString("generic.confirm.archive"),
						bundleMessages.getString("generic.confirm.title"), JOptionPane.YES_NO_OPTION);
				if (resp == 0) {
					final DetailsTableModel pModel = (DetailsTableModel) table.getModel();
					final Tracking tracking = pModel.getTracking(selectedRow);
					EventManager.fire(tracking, new AnnotationLiteral<Archived>() {
					});
				}
			}

		});
		popupMenu.add(menuItem);

		menuItem = new JMenuItem(bundleMessages.getString("popup.table.browse"));
		menuItem.setName("browse");
		menuItem.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.popup.table.browse")));
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final DetailsTableModel pModel = (DetailsTableModel) table.getModel();
				final Tracking tracking = pModel.getTracking(selectedRow);
				EventManager.fire(tracking, new AnnotationLiteral<Browse>() {
				});
			}

		});
		popupMenu.add(menuItem);

		menuItem = new JMenuItem(bundleMessages.getString("popup.table.map"));
		menuItem.setName("browse");
		menuItem.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.popup.table.map")));
		menuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final DetailsTableModel pModel = (DetailsTableModel) table.getModel();
				final Tracking tracking = pModel.getTracking(selectedRow);
				EventManager.fire(tracking, new AnnotationLiteral<Map>() {
				});
			}

		});
		popupMenu.add(menuItem);
	}

	/**
	 * Inicializando a tabela que exibirá as postagens.
	 */
	private void initializeJTable() {
		panel = new JPanel(new BorderLayout());
		table = new JTable() {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(final int row, final int column) {
				if (column == 0) {
					return true;
				}
				return false;
			};
		};

		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setDoubleBuffered(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		final TableCellRenderer defRenderer = table.getTableHeader().getDefaultRenderer();
		table.getTableHeader().setDefaultRenderer(new DefaultTableCellRenderer() {
			private static final long serialVersionUID = 1L;

			@Override
			public Component getTableCellRendererComponent(final JTable table, final Object value,
					final boolean isSelected, final boolean hasFocus, final int row, final int column) {

				final Component c = defRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
						column);

				if (c instanceof DefaultTableCellRenderer) {
					final DefaultTableCellRenderer dtcr = (DefaultTableCellRenderer) c;
					dtcr.setHorizontalAlignment(SwingConstants.CENTER);
				}
				return c;
			}
		});

		table.addMouseListener(new MouseAdapter() {

			@Override
			@SuppressWarnings("serial")
			public void mouseClicked(final MouseEvent event) {
				if (table.getModel() instanceof DetailsTableModel) {
					final DetailsTableModel pModel = (DetailsTableModel) table.getModel();
					final Tracking tracking = pModel.getTracking(table.getSelectedRow());
					if (event.getClickCount() == 2 && tracking != null) {
						EventManager.fire(tracking, new AnnotationLiteral<Modify>() {
						});
					} else {
						if (tracking != null) {
							EventManager.fire(tracking, new AnnotationLiteral<DetailsRequired>() {
							});
						}
					}
				}
			}

		});

		table.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				selectedRow = table.rowAtPoint(e.getPoint());
				showPopup(e);
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				selectedRow = table.rowAtPoint(e.getPoint());
				showPopup(e);
			}

			private void showPopup(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});

		panel.add(table, BorderLayout.CENTER);
		setViewportView(panel);
		setAutoscrolls(true);
	}

	/**
	 * Configurar as propriedades necessárias para cada tipo de célula.
	 */
	private void initializeJTableCellProperties() {

		table.setRowHeight(60);
		table.setShowVerticalLines(false);

		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		table.getColumnModel().getColumn(0).setMaxWidth(40);

		table.getColumnModel().getColumn(1).setPreferredWidth(40);
		table.getColumnModel().getColumn(1).setMaxWidth(40);

		table.getColumnModel().getColumn(2).setPreferredWidth(150);
		table.getColumnModel().getColumn(2).setMaxWidth(150);

		table.getColumnModel().getColumn(3).setPreferredWidth(100);
		table.getColumnModel().getColumn(3).setMaxWidth(100);

		table.getColumnModel().getColumn(4).setPreferredWidth(100);
		table.getColumnModel().getColumn(4).setMaxWidth(100);

		table.getColumnModel().getColumn(5).setPreferredWidth(50);
		table.getColumnModel().getColumn(5).setMaxWidth(50);

		table.getColumnModel().getColumn(6).setPreferredWidth(200);
		table.getColumnModel().getColumn(6).setMaxWidth(200);

		table.getTableHeader().setPreferredSize(new Dimension(0, 35));
	}

	/**
	 * Definir os objetos responsáveis por desenhar cada tipo de célula da tabela.
	 */
	private void initializeJTableCellRenderers() {
		table.getColumnModel().getColumn(0).setCellRenderer(new IconCellRenderer());
		table.getColumnModel().getColumn(1).setCellRenderer(new IconCellRenderer());
		table.getColumnModel().getColumn(2).setCellRenderer(new CodeCellRenderer());
		table.getColumnModel().getColumn(3).setCellRenderer(new TimeCellRenderer());
		table.getColumnModel().getColumn(4).setCellRenderer(new TimeCellRenderer());
		table.getColumnModel().getColumn(5).setCellRenderer(new ElapsedCellRenderer());
		table.getColumnModel().getColumn(6).setCellRenderer(new FromToCellRenderer());
		table.getColumnModel().getColumn(7).setCellRenderer(new DefaultCellRenderer());
	}

	public void setTrackings(final List<Tracking> trackings) {
		table.setModel(new DetailsTableModel(trackings));
		initializeJTableCellRenderers();
		initializeJTableCellProperties();
	}

}