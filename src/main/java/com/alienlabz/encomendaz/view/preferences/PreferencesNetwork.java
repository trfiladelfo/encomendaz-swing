package com.alienlabz.encomendaz.view.preferences;

import java.util.ResourceBundle;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.TitledBorder;

import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.view.component.JRichTextField;

import net.miginfocom.swing.MigLayout;

/**
 * Painel com as preferências do usuário para a rede.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class PreferencesNetwork extends JPanel {
	private static final long serialVersionUID = 1L;
	private ResourceBundle bundleMessages;
	private JCheckBox useProxy;
	private JRichTextField username;
	private JPasswordField password;

	public PreferencesNetwork() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		setLayout(new MigLayout("fillx", "[align left]"));

		initializeFields();
	}

	/**
	 * Inicializar os demais campos do formulário.
	 */
	private void initializeFields() {
		username = FieldFactory.richtext(bundleMessages.getString("preferences.network.username.hint"));
		password = FieldFactory.password();
		useProxy = FieldFactory.checkbox();

		JPanel panel = new JPanel(new MigLayout("fillx", "[align left][align center, grow]"));

		panel.setBorder(new TitledBorder(bundleMessages.getString("preferences.network.proxy.title")));

		panel.add(new JLabel(bundleMessages.getString("preferences.network.useproxy.label")), "gap 10");
		panel.add(useProxy, "span, growx");

		panel.add(new JLabel(bundleMessages.getString("preferences.network.username.label")), "gap 10");
		panel.add(username, "span, growx");

		panel.add(new JLabel(bundleMessages.getString("preferences.network.password.label")), "gap 10");
		panel.add(password, "span, growx");

		add(panel, "span, growx");
	}

	public String getProxyUsername() {
		return username.getText();
	}

	public void setProxyUsername(String username) {
		this.username.setText(username);
	}

	public boolean isUseProxy() {
		return this.useProxy.isSelected();
	}

	public void setUseProxy(boolean use) {
		this.useProxy.setSelected(use);
	}

	public String getProxyPassword() {
		return String.valueOf(password.getPassword());
	}

	public void setProxyPassword(String pas) {
		this.password.setText(pas);
	}

}
