package com.alienlabz.encomendaz.view.component;

import java.text.DecimalFormat;

import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;


/**
 * Campo que recebe apenas valores monetários.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class JMoneyField extends JNumberField {
	private static final long serialVersionUID = 1L;

	public JMoneyField() {
		super(new DecimalFormat("#,##0.00"));
		setIcon(IconUtil.getImageIcon(ResourceBundleFactory.getImagesBundle().getString("default.moneyfield.icon")));
	}

}
