package com.alienlabz.encomendaz.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.ResourceBundle;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXSearchField;
import org.jdesktop.swingx.JXSearchField.LayoutStyle;
import org.jdesktop.swingx.JXSearchField.SearchMode;
import org.jdesktop.swingx.JXStatusBar;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.events.RefreshTrackingsList;
import com.alienlabz.encomendaz.bus.qualifiers.Searched;
import com.alienlabz.encomendaz.configuration.InternalConfiguration;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.domain.TrackingStatus;
import com.alienlabz.encomendaz.factory.ButtonBarFactory;
import com.alienlabz.encomendaz.factory.FieldFactory;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.factory.RibbonFactory;
import com.alienlabz.encomendaz.util.IconUtil;
import com.alienlabz.encomendaz.view.table.JTableTrackings;
import com.alienlabz.encomendaz.view.window.DetailsTabPane;

/**
 * Tela de entrada da aplicação.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class Entrance extends JFrame {
	private static final long serialVersionUID = 1L;
	private JLabel labelNetwork;
	private JXSearchField searchField;
	private JLabel iconNetwork;
	private DetailsTabPane detailsTabPane;
	private JProgressBar progressBar;
	private JPanel mainPanel;
	private JXStatusBar statusBar;
	private JSplitPane splitPane;
	private JTableTrackings tableTrackings;
	public static JLabel statusLabel = new JLabel("");
	private ResourceBundle bundleSystem;
	private ResourceBundle bundleMessages;
	private ResourceBundle bundleImages;
	private JLabel labelListTrackings = new JLabel();

	/**
	 * Iniciar a tela de entrada da aplicação.
	 */
	public void initialize() {
		setPreferredSize(InternalConfiguration.getEntranceDimension());
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

		bundleImages = ResourceBundleFactory.getImagesBundle();
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleSystem = ResourceBundleFactory.getSystemBundle();

		setTitle(bundleMessages.getString("application.title"));

		add(new ButtonBarFactory().build(), BorderLayout.WEST);
		add(new RibbonFactory().build(), BorderLayout.NORTH);

		initializeSplitPane();
		initializeStatusPanel();
		initializeMainPanel();
		initializeMainPanelTopBar();
		initializeDetailsTrackingPane();
		initializeTable();
	}

	/**
	 * Informar na tela que a rede está fora do ar.
	 */
	public void setNetworkOff() {
		try {
			SwingUtilities.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					iconNetwork.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.network.off")));
					labelNetwork.setText(bundleMessages.getString("status.disconnected"));
				}

			});
		} catch (InterruptedException e) {
		} catch (InvocationTargetException e) {
		}
	}

	/**
	 * Informar na tela que a rede está de volta ao ar.
	 */
	public void setNetworkOn() {
		try {
			SwingUtilities.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					iconNetwork.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.network.on")));
					labelNetwork.setText(bundleMessages.getString("status.connected"));
				}

			});
		} catch (InterruptedException e) {
		} catch (InvocationTargetException e) {
		}
	}

	/**
	 * Inicializar o painel inferior da janela.
	 */
	private void initializeDetailsTrackingPane() {
		detailsTabPane = new DetailsTabPane();
		detailsTabPane.setPreferredSize(new Dimension(0, 200));
		splitPane.add(detailsTabPane);
	}

	/**
	 * Inicializar o painel principal.
	 */
	private void initializeMainPanel() {
		mainPanel = new JPanel(new BorderLayout());
		splitPane.add(mainPanel);
	}

	/**
	 * Informar o texto que será exibido como título para a listagem de rastreamentos.
	 * 
	 * @param text Texto que será exibido.
	 */
	public void setTrackingsListDescriptionText(String text) {
		labelListTrackings.setText(text);
	}

	/**
	 * Inicializar o painel superior da janela.
	 */
	private void initializeMainPanelTopBar() {
		final JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(0, 42));
		panel.setLayout(new BorderLayout());
		panel.setBackground(Color.BLACK);

		labelListTrackings.setFont(Font.decode("Tahoma-BOLD-13"));
		labelListTrackings.setForeground(Color.DARK_GRAY);

		final JPanel panelSearch = new JPanel(new FlowLayout());
		panelSearch.setBackground(Color.BLACK);

		final JButton button = FieldFactory.button("");
		button.setIcon(IconUtil.getImageIcon(bundleImages.getString("button.refresh.icon")));
		button.setPreferredSize(new Dimension(33, 33));
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				EventManager.fire(new RefreshTrackingsList());
			}

		});

		searchField = new JXSearchField();
		searchField.setLayoutStyle(LayoutStyle.MAC);
		searchField.setPreferredSize(new Dimension(300, searchField.getPreferredSize().height));
		searchField.setSearchMode(SearchMode.REGULAR);
		searchField.setInstantSearchDelay(200);
		searchField.setRecentSearchesSaveKey("net.encomendaz.preferences.search");
		searchField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventManager.fire(searchField.getText(), new AnnotationLiteral<Searched>() {
				});
			}
		});

		panelSearch.add(button);
		panelSearch.add(searchField);

		panel.add(labelListTrackings, BorderLayout.WEST);
		panel.add(panelSearch, BorderLayout.EAST);
		mainPanel.add(panel, BorderLayout.NORTH);
	}

	/**
	 * Inicializar o painel que separa a lista de rastreamentos dos detalhes através de um painel redimensionável.
	 */
	private void initializeSplitPane() {
		splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		splitPane.setDividerLocation(450);
		splitPane.setOneTouchExpandable(true);
		add(splitPane, BorderLayout.CENTER);
	}

	/**
	 * Inicializar o painel de status da aplicação.
	 */
	private void initializeStatusPanel() {
		statusBar = new JXStatusBar();
		statusLabel.setFont(Font.decode(bundleSystem.getString("statusbar.label.font")));
		statusBar.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, Color.LIGHT_GRAY));

		final JXStatusBar.Constraint c1 = new JXStatusBar.Constraint();
		c1.setFixedWidth(100);
		statusBar.add(statusLabel, c1);
		final JXStatusBar.Constraint c2 = new JXStatusBar.Constraint(JXStatusBar.Constraint.ResizeBehavior.FILL);
		progressBar = new JProgressBar();
		statusBar.add(progressBar, c2);

		final JXStatusBar.Constraint c3 = new JXStatusBar.Constraint();

		final JPanel panelNetwork = new JPanel();
		labelNetwork = new JLabel();
		labelNetwork.setFont(Font.decode("Tahoma-Normal-10"));
		labelNetwork.setText(bundleMessages.getString("status.connected"));

		iconNetwork = new JLabel();
		iconNetwork.setIcon(IconUtil.getImageIcon(bundleImages.getString("icon.network.on")));
		panelNetwork.add(labelNetwork);
		panelNetwork.add(iconNetwork);

		statusBar.add(panelNetwork, c3);
		add(statusBar, BorderLayout.SOUTH);
	}

	/**
	 * Inicializar a tabela que conterá as postagens.
	 */
	private void initializeTable() {
		tableTrackings = new JTableTrackings();
		mainPanel.add(tableTrackings, BorderLayout.CENTER);
	}

	/**
	 * Colocar a statusbar para indicar que não há mais trabalho em progresso.
	 */
	synchronized public void setStatusBarAsStopped() {
		progressBar.setIndeterminate(false);
		progressBar.repaint();
		statusLabel.setText(bundleMessages.getString("statusbar.label.stopped.text"));
	}

	/**
	 * Colocar a statusbar para indicar que há trabalho em progresso.
	 */
	synchronized public void setStatusBarAsWorking() {
		progressBar.setIndeterminate(true);
		progressBar.repaint();
		statusLabel.setText(bundleMessages.getString("statusbar.label.working.text"));
	}

	/**
	 * Exibir os detalhes de um rastreamento.
	 * 
	 * @param tracking Rastreamento.
	 */
	public void setStatusesDetails(final List<TrackingStatus> statuses) {
		detailsTabPane.setStatuses(statuses);
	}

	/**
	 * Exibir para o usuário a tabela contendo a lista de todos os rastreamentos.
	 * 
	 * @param trackings Rastreamentos que serão exibidos.
	 */
	public void showTrackings(final List<Tracking> trackings) {
		tableTrackings.setTrackings(trackings);
	}

	public void setCode(String code) {
		this.detailsTabPane.setCode(code);
	}

	public void setLastStatus(String s) {
		this.detailsTabPane.setLastStatus(s);
	}

	public void setDescription(String d) {
		this.detailsTabPane.setDescription(d);
	}

	public void setFromDetails(String f) {
		this.detailsTabPane.setFromDetails(f);
	}

	public void setToDetails(String d) {
		this.detailsTabPane.setToDetails(d);
	}

	public void setEstimative(String e) {
		this.detailsTabPane.setEstimative(e);
	}

	public String getSearchText() {
		return searchField.getText();
	}

}
