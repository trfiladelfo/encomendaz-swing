package com.alienlabz.encomendaz.view.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import com.alienlabz.encomendaz.domain.Backup;


/**
 * Model para ser utilizado nas listagens de backups.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class BackupListModel implements ListModel {
	private List<Backup> list = new ArrayList<Backup>();

	public BackupListModel(List<Backup> list) {
		this.list = list;
	}

	@Override
	public void addListDataListener(ListDataListener l) {
	}

	@Override
	public Object getElementAt(int index) {
		return list.get(index);
	}

	@Override
	public int getSize() {
		return list.size();
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
	}

}
