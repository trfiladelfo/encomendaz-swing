package com.alienlabz.encomendaz.util;

import java.awt.Dimension;
import java.awt.Image;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.pushingpixels.flamingo.api.common.icon.ImageWrapperResizableIcon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

/**
 * Utilitário para tratar informações sobre ícones da aplicação.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
final public class IconUtil {

	/**
	 * Obter um ícone a partir do seu nome.
	 * 
	 * @param imageName Nome do ícone.
	 * @return Ícone.
	 */
	public static Icon getImageIcon(final String imageName) {
		return new ImageIcon(IconUtil.class.getClassLoader().getResource("icons/" + imageName));
	}

	/**
	 * Verificar se o ícone informado existe.
	 * 
	 * @param icon Ícone a ser procurado.
	 * @return Verdadeiro, caso exista.
	 */
	public static boolean existsIcon(final String icon) {
		boolean result = true;
		try {
			result = IconUtil.class.getClassLoader().getResource("icons/" + icon) != null ? true : false;
		} catch (Exception exception) {
			result = false;
		}
		return result;
	}

	/**
	 * Obter uma imagem do sistema.
	 * 
	 * @param imageName Nome da imagem.
	 * @return Imagem.
	 */
	public static Icon getImageIcon(final String imageName, final String path) {
		return new ImageIcon(IconUtil.class.getClassLoader().getResource(path + "/" + imageName));
	}

	/**
	 * Obter uma imagem do sistema.
	 * 
	 * @param imageName Nome da imagem.
	 * @return Imagem.
	 */
	public static Image getImage(final String imageName) {
		return new ImageIcon(IconUtil.class.getClassLoader().getResource("icons/" + imageName)).getImage();
	}

	/**
	 * Obter um ícone redimensionável para ser usada pelos componentes do Flamingo.
	 * 
	 * @param iconName Nome do ícone.
	 * @return Ícone redimensionável.
	 */
	public static ResizableIcon getResizableIcon(final String iconName, final Dimension dimension) {
		return ImageWrapperResizableIcon.getIcon(IconUtil.class.getClassLoader().getResource("icons/" + iconName), dimension);
	}

}
