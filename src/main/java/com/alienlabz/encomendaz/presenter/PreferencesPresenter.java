package com.alienlabz.encomendaz.presenter;

import java.awt.Desktop;
import java.io.File;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.slf4j.Logger;

import br.gov.frameworkdemoiselle.annotation.Name;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.util.ResourceBundle;

import com.alienlabz.encomendaz.bus.events.ChangePrefs;
import com.alienlabz.encomendaz.bus.events.EmailServerChanged;
import com.alienlabz.encomendaz.bus.events.TestEmailConnection;
import com.alienlabz.encomendaz.bus.qualifiers.Browse;
import com.alienlabz.encomendaz.bus.qualifiers.Closed;
import com.alienlabz.encomendaz.domain.EmailServerSecurity;
import com.alienlabz.encomendaz.domain.EmailServers;
import com.alienlabz.encomendaz.message.Messages;
import com.alienlabz.encomendaz.preferences.UserPreferences;
import com.alienlabz.encomendaz.util.AsyncTask;
import com.alienlabz.encomendaz.util.MailSender;
import com.alienlabz.encomendaz.util.NumberUtil;
import com.alienlabz.encomendaz.util.SwingUtil;
import com.alienlabz.encomendaz.view.preferences.PreferencesDialog;

/**
 * Controlador da visão de preferências.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@Singleton
@ViewController
public class PreferencesPresenter {

	@Inject
	private Logger logger;

	@Inject
	private UserPreferences prefs;

	@Inject
	@Name("messages")
	private ResourceBundle bundleMessages;

	@Inject
	private MailSender mail;

	private PreferencesDialog view;

	/**
	 * Capturar o evento que solicita a exibição da janela de preferências.
	 * 
	 * @param event Evento.
	 */
	public void viewPreferences(@Observes final ChangePrefs event) {
		logger.debug("exibir a janela de preferências.");

		view = new PreferencesDialog();

		// Preencher campos.
		view.setEmailSecurityOptions(EmailServerSecurity.values());
		view.setEmailOptions(EmailServers.values());

		// Definir os valores padrão.
		view.setRefreshRate(prefs.getRefreshRate());
		view.setNotificationSound(prefs.getNotificationSound());
		view.setEmailPassword(prefs.getEmailPassword());
		view.setEmailSecurity(prefs.getEmailSecurity());
		view.setEmailServer(prefs.getEmailServer());
		view.setEmailServerPort(prefs.getEmailServerPort());
		view.setEmailTemplate(prefs.getEmailTemplate());
		view.setEmailUsername(prefs.getEmailUsername());
		view.setEmailSender(prefs.getEmailSender());
		view.setEmailSubject(prefs.getEmailSubject());
		view.setAvailableThemes(SubstanceLookAndFeel.getAllSkins());
		view.setCurrentTheme(SubstanceLookAndFeel.getCurrentSkin().getDisplayName());
		view.setProxyPassword(prefs.getProxyPassword());
		view.setProxyUsername(prefs.getProxyUsername());
		view.setUseProxy(prefs.isUseProxy());
		view.setSendAutomaticEmails(prefs.isSendAutomaticEmails());

		view.pack();
		SwingUtil.center(view);
		view.setVisible(true);
	}

	/**
	 * O usuário deseja ver o arquivo de template de e-mail.
	 * 
	 * @param file Template.
	 */
	public void browseTemplate(@Observes @Browse String file) {
		try {
			Desktop.getDesktop().open(new File(file));
		} catch (Throwable e) {
			Messages.error(bundleMessages.getString("exception.browse.template"));
			e.printStackTrace();
		}
	}

	/**
	 * O servidor de e-mail foi mudado.
	 * 
	 * @param event Evento.
	 */
	public void emailServerChanged(@Observes EmailServerChanged event) {
		event.getPreferencesEmail().setEmailPassword("");
		event.getPreferencesEmail().setEmailSecurity(event.getServer().getSecurity());
		event.getPreferencesEmail().setEmailServer(event.getServer().getSmtp());
		event.getPreferencesEmail().setEmailServerPort(event.getServer().getPort());
		event.getPreferencesEmail().setEmailUsername("");
	}

	/**
	 * Testar a conexão com o servidor de e-mail informado.
	 * 
	 * @param event Evento.
	 */
	public void testEmailConnection(@Observes TestEmailConnection event) {
		
		if(view.getEmailSender() == null) {
			Messages.error(bundleMessages.getString("error.mail.test.no.sender"));
			return;
		}
		
		new AsyncTask<Boolean, Void>() {

			@Override
			protected Boolean doTask() {
				view.disableEmailTest();
				try {
					mail.send(view.getEmailServer(), NumberUtil.toInteger(view.getEmailServerPort()), view.getEmailUsername(), view.getEmailPassword(), view.getEmailSecurity(),
							view.getEmailSender());
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
				return true;
			}

			@Override
			protected void onPostExecute(Boolean result) {
				if (!result) {
					view.enableEmailTest();
					Messages.error(bundleMessages.getString("error.mail.test"));
				} else {
					Messages.info(bundleMessages.getString("success.mail.test"));
				}
			}

		}.execute();
	}

	/**
	 * Capturar o evento que fecha a janela de preferências e salvar os dados digitados pelo usuário.
	 * 
	 * @param dialog Janela de Preferências.
	 */
	public void closed(@Observes @Closed PreferencesDialog dialog) {
		logger.debug("janela de preferências foi fechada. salvar os dados.");

		prefs.setRefreshRate(dialog.getRefreshRate());
		prefs.setNotificationSound(dialog.getNotificationSound());
		prefs.setEmailPassword(dialog.getEmailPassword());
		prefs.setEmailSecurity(dialog.getEmailSecurity());
		prefs.setEmailServer(dialog.getEmailServer());
		prefs.setEmailServerPort(NumberUtil.toInteger(dialog.getEmailServerPort()));
		prefs.setEmailTemplate(dialog.getEmailTemplate());
		prefs.setEmailUsername(dialog.getEmailUsername());
		prefs.setEmailSubject(dialog.getEmailSubject());
		prefs.setEmailSender(dialog.getEmailSender());
		prefs.setDefaultSkin(SubstanceLookAndFeel.getAllSkins().get(dialog.getTheme()).getClassName());
		prefs.setProxyPassword(dialog.getProxyPassword());
		prefs.setProxyUsername(dialog.getProxyUsername());
		prefs.setUseProxy(dialog.isUseProxy());
		prefs.setSendAutomaticEmails(dialog.isSendAutomaticEmails());
		prefs.store();
	}

}
