package com.alienlabz.encomendaz.presenter;

import java.net.URL;
import java.util.List;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;

import br.gov.frameworkdemoiselle.annotation.Name;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.util.ResourceBundle;

import com.alienlabz.encomendaz.bus.events.CloseApplication;
import com.alienlabz.encomendaz.bus.events.ListDelivered;
import com.alienlabz.encomendaz.bus.events.ListImportants;
import com.alienlabz.encomendaz.bus.events.ListInTransit;
import com.alienlabz.encomendaz.bus.events.ListNotArchived;
import com.alienlabz.encomendaz.bus.events.ListNotDelivered;
import com.alienlabz.encomendaz.bus.events.ListRecent;
import com.alienlabz.encomendaz.bus.events.ListUnregistered;
import com.alienlabz.encomendaz.bus.events.NetworkOff;
import com.alienlabz.encomendaz.bus.events.NetworkOn;
import com.alienlabz.encomendaz.bus.events.RefreshTrackingsList;
import com.alienlabz.encomendaz.bus.events.ShowAbout;
import com.alienlabz.encomendaz.bus.events.Stopped;
import com.alienlabz.encomendaz.bus.events.Working;
import com.alienlabz.encomendaz.bus.qualifiers.AfterDeleted;
import com.alienlabz.encomendaz.bus.qualifiers.AfterSaved;
import com.alienlabz.encomendaz.bus.qualifiers.Archived;
import com.alienlabz.encomendaz.bus.qualifiers.DetailsRequired;
import com.alienlabz.encomendaz.bus.qualifiers.DisplayApplication;
import com.alienlabz.encomendaz.bus.qualifiers.Searched;
import com.alienlabz.encomendaz.bus.qualifiers.StatusChanged;
import com.alienlabz.encomendaz.business.TrackingBC;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.exceptions.UIException;
import com.alienlabz.encomendaz.preferences.UserPreferences;
import com.alienlabz.encomendaz.util.AsyncTask;
import com.alienlabz.encomendaz.util.AudioPlayer;
import com.alienlabz.encomendaz.util.SearchBundle;
import com.alienlabz.encomendaz.util.SwingUtil;
import com.alienlabz.encomendaz.view.Entrance;
import com.alienlabz.encomendaz.view.tray.TrayIconManager;
import com.alienlabz.encomendaz.view.window.AboutForm;

/**
 * Responsável em tratar os eventos e a inicialização da tela de entrada do sistema.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@ViewController
@Singleton
public class MainPresenter {
	private static final short ALL = 1;
	private static final short DELIVERED = 2;
	private static final short INTRANSIT = 3;
	private static final short UNREGISTERED = 4;
	private static final short RECENTS = 5;
	private static final short IMPORTANTS = 6;
	private static final short NOT_DELIVERED = 7;
	private Entrance entrance;
	private short workingCount = 0;
	private short whichList = ALL;
	private Tracking lastTracking = null;

	@Inject
	private UserPreferences userPreferences;

	@Inject
	private TrackingBC trackingBC;

	@Inject
	@Name("messages")
	private ResourceBundle bundleMessages;

	@Inject
	@Name("system")
	private ResourceBundle bundleSystem;

	@Inject
	private Logger logger;

	/**
	 * Capturar o evento que informa que a rede saiu do ar.
	 * 
	 * @param event Evento.
	 */
	public void networkOff(@Observes NetworkOff event) {
		entrance.setNetworkOff();
	}

	/**
	 * Capturar o evento que informa que a rede está no ar.
	 * 
	 * @param event Evento.
	 */
	public void networkOn(@Observes NetworkOn event) {
		entrance.setNetworkOn();
	}

	/**
	 * Observar o evento que solicita a pesquisa de rastreamentos.
	 * 
	 * @param text Texto a ser pesquisado.
	 */
	public void searched(@Observes @Searched final String text) {
		statusChanged(null);
	}

	/**
	 * Capturar o evento que solicita a atualização da lista de trackings.
	 * 
	 * @param event Evento.
	 */
	public void refreshTrackings(@Observes final RefreshTrackingsList event) {
		new AsyncTask<Void, Void>() {
			@Override
			protected Void doTask() {
				trackingBC.verifyTrackings();
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				statusChanged(new Tracking());
			}
		}.execute();
	}

	/**
	 * Tratar o evento lançado após ocorrer o salvamento de um rastreamento.
	 * 
	 * @param tracking Rastreamento salvo.
	 */
	public void afterSaveTracking(@Observes @AfterSaved final Tracking tracking) {
		statusChanged(null);
	}

	/**
	 * Capturar o evento que é lançado após a exclusão de um rastreamento.
	 * 
	 * @param tracking Rastreamento removido.
	 */
	public void afterDeleteTracking(@Observes @AfterDeleted final Tracking tracking) {
		statusChanged(null);
	}

	/**
	 * Capturar o evento que informa que o sistema deve apresentar-se livre.
	 */
	public void free(@Observes final Stopped event) {
		workingCount--;
		if (workingCount <= 0) {
			entrance.setStatusBarAsStopped();
			workingCount = 0;
		}
	}

	/**
	 * Capturar o evento que informa que o sistema deve apresentar-se ocupado.
	 */
	public void occupied(@Observes final Working event) {
		workingCount++;
		entrance.setStatusBarAsWorking();
	}

	/**
	 * Capturar o evento que solicita o arquivamento de uma encomenda.
	 * 
	 * @param tracking Encomenda a ser arquivada.
	 */
	public void archived(@Observes @Archived final Tracking tracking) {
		trackingBC.archive(tracking);
		statusChanged(null);
	}

	/**
	 * Capturar o evento que informa que ocorreu mudança de situação em postagens.
	 * 
	 * @param trackings Postagens que foram modificadas.
	 */
	public void statusChanged(@Observes @StatusChanged final Tracking trackings) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				switch (whichList) {
				case ALL:
					listNotArchivedTrackings(null);
					break;
				case DELIVERED:
					listDelivered(null);
					break;
				case INTRANSIT:
					listInTransit(null);
					break;
				case RECENTS:
					break;
				case UNREGISTERED:
					listUnregistered(null);
					break;
				case IMPORTANTS:
					listImportants(null);
					break;
				}
			}

		});
	}

	/**
	 * Capturar o evento que informa que ocorreu mudança de situação em postagens.
	 * 
	 * @param trackings Postagens que foram modificadas.
	 */
	public void statusChangedFromJob(@Observes @StatusChanged Tracking trackings) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				switch (whichList) {
				case ALL:
					listNotArchivedTrackings(null);
					break;
				case DELIVERED:
					listDelivered(null);
					break;
				case INTRANSIT:
					listInTransit(null);
					break;
				case RECENTS:
					break;
				case UNREGISTERED:
					listUnregistered(null);
					break;
				case IMPORTANTS:
					listImportants(null);
					break;
				}
				TrayIconManager.showMessage(bundleMessages.getString("status.changed"));
				playNotificationSound();
			}

		});
	}

	private void playNotificationSound() {
		if (userPreferences.getNotificationSound() != null && !"".equals(userPreferences.getNotificationSound())) {
			try {
				AudioPlayer.play(userPreferences.getNotificationSound());
			} catch (Throwable e) {
				logger.error("Erro", e);
			}
		} else {
			URL url = getClass().getClassLoader().getResource(
					"./sounds/" + bundleSystem.getString("default.sound.file"));
			try {
				AudioPlayer.play(url.openStream());
			} catch (Throwable e) {
				logger.error("Erro", e);
			}
		}
	}

	/**
	 * Capturar o evento que solicita a abertura da janela principal da aplicação.
	 * 
	 * @param string
	 */
	public void displayEntrance(@Observes @DisplayApplication String string) {
		entrance.setVisible(true);
	}

	/**
	 * Exibir a tela de entrada.
	 */
	public void showEntrance() {
		if (entrance == null) {
			entrance = new Entrance();
		}

		entrance.initialize();
		entrance.pack();

		entrance.setVisible(true);
		SwingUtil.center(entrance);

		new AsyncTask<Void, Void>() {

			@Override
			protected Void doTask() {
				listNotArchivedTrackings(null);
				return null;
			}
		}.execute();

	}

	/**
	 * Obter os dados necessários para preencher o Bundle de pesquisa.
	 * 
	 * @return Bundle de pesquisa.
	 */
	private SearchBundle getSearchBundle() {
		SearchBundle bundle = new SearchBundle();
		bundle.setSearchText(entrance.getSearchText());
		return bundle;
	}

	/**
	 * Tratar o evento lançado solicitando a exibição dos rastreamentos importantes.
	 * 
	 * @param event Evento.
	 */
	public void listImportants(@Observes final ListImportants event) {
		whichList = IMPORTANTS;
		new AsyncTask<List<Tracking>, Void>() {

			@Override
			protected List<Tracking> doTask() {
				return trackingBC.findImportantTrackings(getSearchBundle());
			}

			@Override
			protected void onPostExecute(List<Tracking> list) {
				try {
					entrance.showTrackings(list);
					entrance.setTrackingsListDescriptionText("  " + bundleMessages.getString("trackings.important")
							+ " - Total: " + list.size());
				} catch (final Exception e) {
					throw new UIException(e);
				}
			}

		}.execute();
	}

	/**
	 * Tratar o evento lançado solicitando a exibição dos rastreamentos importantes.
	 * 
	 * @param event Evento.
	 */
	public void listOverdue(@Observes final ListNotDelivered event) {
		whichList = NOT_DELIVERED;
		new AsyncTask<List<Tracking>, Void>() {

			@Override
			protected List<Tracking> doTask() {
				return trackingBC.findOverdueTrackings(getSearchBundle());
			}

			@Override
			protected void onPostExecute(List<Tracking> list) {
				try {
					entrance.showTrackings(list);
					entrance.setTrackingsListDescriptionText("  " + bundleMessages.getString("trackings.notdelivered")
							+ " - Total: " + list.size());
				} catch (final Exception e) {
					throw new UIException(e);
				}
			}

		}.execute();
	}

	/**
	 * Tratar o evento lançado solicitando a exibição dos rastreamentos importantes.
	 * 
	 * @param event Evento.
	 */
	public void listRecents(@Observes final ListRecent event) {
		whichList = RECENTS;
		new AsyncTask<List<Tracking>, Void>() {

			@Override
			protected List<Tracking> doTask() {
				return trackingBC.findRecentTrackings(getSearchBundle());
			}

			@Override
			protected void onPostExecute(List<Tracking> list) {
				try {
					entrance.showTrackings(list);
					entrance.setTrackingsListDescriptionText("  " + bundleMessages.getString("trackings.recent")
							+ " - Total: " + list.size());
				} catch (final Exception e) {
					throw new UIException(e);
				}
			}

		}.execute();
	}

	/**
	 * Tratar o evento lançado solicitando a exibição dos rastreamentos que estão em trânsito.
	 * 
	 * @param event Evento.
	 */
	public void listInTransit(@Observes final ListInTransit event) {
		whichList = INTRANSIT;
		new AsyncTask<List<Tracking>, Void>() {

			@Override
			protected List<Tracking> doTask() {
				return trackingBC.findInTransitTrackings(getSearchBundle());
			}

			@Override
			protected void onPostExecute(List<Tracking> list) {
				try {
					entrance.showTrackings(list);
					entrance.setTrackingsListDescriptionText("  " + bundleMessages.getString("trackings.intransit")
							+ " - Total: " + list.size());
				} catch (final Exception e) {
					throw new UIException(e);
				}
			}

		}.execute();
	}

	/**
	 * Tratar o evento lançado solicitando a exibição dos rastreamentos entregues.
	 * 
	 * @param event Evento.
	 */
	public void listDelivered(@Observes final ListDelivered event) {
		whichList = DELIVERED;
		new AsyncTask<List<Tracking>, Void>() {

			@Override
			protected List<Tracking> doTask() {
				return trackingBC.findDeliveredTrackings(getSearchBundle());
			}

			@Override
			protected void onPostExecute(List<Tracking> list) {
				try {
					entrance.showTrackings(list);
					entrance.setTrackingsListDescriptionText("  " + bundleMessages.getString("trackings.delivered")
							+ " - Total: " + list.size());
				} catch (final Exception e) {
					throw new UIException(e);
				}
			}

		}.execute();
	}

	/**
	 * Tratar o evento lançado solicitando a exibição dos rastreamentos não registrados.
	 * 
	 * @param event Evento.
	 */
	public void listUnregistered(@Observes final ListUnregistered event) {
		whichList = UNREGISTERED;
		new AsyncTask<List<Tracking>, Void>() {

			@Override
			protected List<Tracking> doTask() {
				return trackingBC.findUnregisteredTrackings(getSearchBundle());
			}

			@Override
			protected void onPostExecute(List<Tracking> list) {
				try {
					entrance.showTrackings(list);
					entrance.setTrackingsListDescriptionText("  " + bundleMessages.getString("trackings.unregistered")
							+ " - Total: " + list.size());
				} catch (final Exception e) {
					throw new UIException(e);
				}
			}

		}.execute();
	}

	/**
	 * Tratar o evento lançado solicitando a exibição dos rastreamentos não arquivados.
	 * 
	 * @param event Evento.
	 */
	public void listNotArchivedTrackings(@Observes final ListNotArchived event) {
		whichList = ALL;
		new AsyncTask<List<Tracking>, Void>() {

			@Override
			protected List<Tracking> doTask() {
				return trackingBC.findNotArchivedTrackings(getSearchBundle());
			}

			@Override
			protected void onPostExecute(List<Tracking> list) {
				try {
					entrance.showTrackings(list);
					entrance.setTrackingsListDescriptionText("  " + bundleMessages.getString("trackings.not.archived")
							+ " - Total: " + list.size());
				} catch (final Exception e) {
					throw new UIException(e);
				}
			}

		}.execute();
	}

	/**
	 * Exibir a janela de About do Sistema.
	 * 
	 * @param event
	 */
	public void showAbout(@Observes final ShowAbout event) {
		AboutForm about = new AboutForm();
		about.pack();
		SwingUtil.center(about);
		about.setVisible(true);
	}

	/**
	 * Tratar o evento que pede a exibição dos detalhes de um rastreamento.
	 * 
	 * @param event Evento.
	 */
	public void trackingDetailsRequired(@Observes @DetailsRequired final Tracking tracking) {
		if (lastTracking != null && tracking.getId().equals(lastTracking.getId())) {
			return;
		}
		lastTracking = tracking;
		entrance.setStatusesDetails(tracking.getStatuses());
		entrance.setDescription(tracking.getDescription());
		entrance.setLastStatus(tracking.getLastStatus() != null ? tracking.getLastStatus().getDetails() : "");
		entrance.setEstimative(tracking.getEstimative() != null ? tracking.getEstimative().toString() : bundleMessages
				.getString("default.text.notinformed"));
		entrance.setCode(tracking.getCode());
		if (tracking.getFrom() != null) {
			entrance.setFromDetails(tracking.getFrom().getCompleteInformations());
		} else {
			entrance.setFromDetails("");
		}
		if (tracking.getTo() != null) {
			entrance.setToDetails(tracking.getTo().getCompleteInformations());
		} else {
			entrance.setToDetails("");
		}
	}

	public Entrance getEntrance() {
		return entrance;
	}

	/**
	 * Tratar o evento que pede para a aplicação ser fechada.
	 * 
	 * @param event Evento.
	 */
	public void closeApplication(@Observes final CloseApplication event) {
		System.exit(0);
	}

}
